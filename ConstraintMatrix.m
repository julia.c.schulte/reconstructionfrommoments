function [A,b]=ConstraintMatrix(n,Theta,scale)
%Define vector c of constraint inequalities (constraint c(indx)<=0 for each element)
%and vector ceq of constraint inequalities (constraint c(indx)=0 for each
%element)


A= sparse(n*5,n);
b=sparse(n*5,1);



%Definition of constraints: 
[a,b1,c,d,e,f,g]=ConditionCoeffs(n,Theta);

indx=0;
for i=1:n
    for j=1:5     
        indx=indx+1;
        %each c(indx) defines the constraint c(indx)<=0
                if j==1               
                       if i<=n-2
                            %c(indx)=-a(i)*h(i)-b(i)*h(i+1)-c_(i)*h(i+2);
                            A(indx,i)=-a(i);
                            A(indx,i+1)=-b1(i);
                            A(indx,i+2)=-c(i);
                       elseif i==n-1
                            %c(indx)=-a(i)*h(i)-b(i)*h(i+1)-c_(i)*h(1);
                            A(indx,i)=-a(i);
                            A(indx,i+1)=-b1(i);
                            A(indx,1)=-c(i);
                       elseif i==n
                            %c(indx)=-a(i)*h(i)-b(i)*h(1)-c_(i)*h(2);
                            A(indx,i)=-a(i);
                            A(indx,1)=-b1(i);
                            A(indx,2)=-c(i);
                       end

                elseif j==2
                       %Coef=[d(i); e(i)];
                       if i<=n-1
                            %c(indx)= -d(i)*h(i)-e(i)*h(i+1);
                            A(indx,i)=-d(i);
                            A(indx,i+1)=-e(i);
                       elseif i==n
                            %c(indx)= -d(i)*h(i)-e(i)*h(1);
                            A(indx,i)=-d(i);
                            A(indx,1)=-e(i);
                       end
                 elseif j==3
                       %Coef=[1; -d(i); -e(i)];
                       if i<=n-1
                           %c(indx)=-scale+d(i)*h(i)+e(i)*h(i+1);
                            A(indx,i)=d(i);
                            A(indx,i+1)=e(i);
                            b(indx)=scale;
                       elseif i==n
                            %c(indx)=-scale+d(i)*h(i)+e(i)*h(1);
                            A(indx,i)=d(i);
                            A(indx,1)=e(i);
                            b(indx)=scale;
                       end   
                 elseif j==4
                       %Coef=[f(i); g(i)];
                       if i<=n-1
                            %c(indx)= -f(i)*h(i)-g(i)*h(i+1);
                            A(indx,i)=-f(i);
                            A(indx,i+1)=-g(i);
                       elseif i==n
                            %c(indx)= -f(i)*h(i)-g(i)*h(1);
                            A(indx,i)=-f(i);
                            A(indx,1)=-g(i);
                       end
                 elseif j==5
                       %Coef=[1; -f(i); -g(i)];
                       if i<=n-1
                            %c(indx)=-scale+f(i)*h(i)+g(i)*h(i+1);
                            A(indx,i)=f(i);
                            A(indx,i+1)=g(i);
                            b(indx)=scale;
                       elseif i==n
                            %c(indx)=-scale+f(i)*h(i)+g(i)*h(1);
                            A(indx,i)=f(i);
                            A(indx,1)=g(i);
                            b(indx)=scale;

                       end
                end
        end
    end 
end



