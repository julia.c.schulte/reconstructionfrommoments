function ConstraintsPlusGrad(n,Theta,scale)
%Define vector c of constraint inequalities (constraint c(indx)<=0 for each element)
%and vector ceq of constraint inequalities (constraint c(indx)=0 for each
%element)

h = cell(1, n);
for i = 1:n
       h{i} = sprintf('h%d',i);
end
h = h(:); % now h is a n-by-1 vector
h = sym(h, 'real');

ceq = [];
gradceq = [];

c = sym(zeros(1,n*5));
gradc=sym(zeros(n*5,n));
%Definition of constraints: 
[a,b,c_,d,e,f,g]=ConditionCoeffs(n,Theta)

indx=0;
for i=1:n
    for j=1:5     
        indx=indx+1;
        %each c(indx) defines the constraint c(indx)<=0
        if j==1               
               if i<=n-2
                    c(indx)=-a(i)*h(i)-b(i)*h(i+1)-c_(i)*h(i+2);
               elseif i==n-1
                    c(indx)=-a(i)*h(i)-b(i)*h(i+1)-c_(i)*h(1);
               elseif i==n
                    c(indx)=-a(i)*h(i)-b(i)*h(1)-c_(i)*h(2);
               end

        elseif j==2
               Coef=[d(i); e(i)]
               if i<=n-1
                    c(indx)= -d(i)*h(i)-e(i)*h(i+1);
               elseif i==n
                    c(indx)= -d(i)*h(i)-e(i)*h(1);
               end
         elseif j==3
               Coef=[1; -d(i); -e(i)]
               if i<=n-1
                   c(indx)=-scale+d(i)*h(i)+e(i)*h(i+1);
               elseif i==n
                   c(indx)=-scale+d(i)*h(i)+e(i)*h(1);
               end   
         elseif j==4
               Coef=[f(i); g(i)]
               if i<=n-1
                    c(indx)= -f(i)*h(i)-g(i)*h(i+1);
               elseif i==n
                    c(indx)= -f(i)*h(i)-g(i)*h(1);
               end
         elseif j==5
               Coef=[1; -f(i); -g(i)]
               if i<=n-1
                    c(indx)=-scale+f(i)*h(i)+g(i)*h(i+1);
               elseif i==n
                    c(indx)=-scale+f(i)*h(i)+g(i)*h(1);
               end
        end
    end 
end


gradc = jacobian(c,h).'

hessc = cell(1, 5*n);
for i = 1:5*n
    hessc{i} = jacobian(gradc(:,i),h);
end

    currdir = [pwd filesep]; 
    filename = [currdir,'constr.m'];
    matlabFunction(c,[],gradc,[],'file',filename,'vars',{h},...
    'outputs',{'c','ceq','gradc','gradceq'});

for i = 1:5*n
    ii = num2str(i);
    thename = ['hessc',ii];
    filename = [currdir,thename,'.m'];
    matlabFunction(hessc{i},'file',filename,'vars',{h});
end


end