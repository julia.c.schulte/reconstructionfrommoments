function C=LegendreCoefficientMatrix(N,scale)

% N - maximal degree of Legendre polynomials

syms n x

%Define shifted and rescaled Legendre polynomials (which are ONB on [0,1]
C=sparse(N+1,N+1);

syms L(m,x)
L(m,x)=sqrt((2*m+1)/scale)*legendreP(m,(2/scale)*x-1);

for n=0:N
    C(n+1,1:(n+1))=fliplr(coeffs(L(n,x),'All'));
end

end