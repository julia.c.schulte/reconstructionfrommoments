function V=PlotPolygon(n,h,Theta,normals)

    %Plot Results
    h=[h,h(1)];
    Cos=cos(Theta);
    Sin=sin(Theta);
    Cos=[Cos,Cos(2)]; Sin=[Sin,Sin(2)];
    V=zeros(2,n+1);
    for i=1:n
        V(1,i)=1/(Cos(i)*Sin(i+1)-Sin(i)*Cos(i+1))*(h(i)*Sin(i+1)-h(i+1)*Sin(i));
        V(2,i)=1/(Cos(i)*Sin(i+1)-Sin(i)*Cos(i+1))*(h(i+1)*Cos(i)-h(i)*Cos(i+1));
    end
    V(1,n+1)=V(1,1);
    V(2,n+1)=V(2,1);

    
    axis equal
    if normals==1
        for i=1:n
            hold on
            %outer normal u_{i+1}
            plot(0.5*(V(1,i)+V(1,i+1))+[0 Cos(i+1)],0.5*(V(2,i)+V(2,i+1))+[0 Sin(i+1)],'r-');
            %if h(i)<0
            %    plot([0 -Cos(i)],[0 -Sin(i)],'k-');
            %end
            %support line H(u_i)
            plot(h(i)*Cos(i)+[0, -Sin(i), Sin(i)],h(i)*Sin(i)+[0,Cos(i),-Cos(i)],'g-');

        end
    end
    %polygon boundary
    plot([V(1,:),V(1,1)],[V(2,:),V(2,1)],'*--k')

end
