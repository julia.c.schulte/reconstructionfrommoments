function prob=DefineProb(N,n,Theta,scale,Legendre)

    if Legendre
        global LegMomK
        global scale
        global LegMomC
    else
        global M
        global GeoMomK
    end

    
h=optimvar('h',1,n);
%Define objective

if Legendre
        f=0;
        for k=0:N
            for l=0:N
                LegMomP_kl=0;
                for i=1:n
                    for s=0:k+l
                        for q1=0:s+1
                            for q2=0:s+2-q1
                                if i<=n-2
                                    LegMomP_kl=LegMomP_kl+LegMomC(k+1,l+1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(i+2)^(s+2-q1-q2);
                                elseif i==n-1
                                    LegMomP_kl=LegMomP_kl+LegMomC(k+1,l+1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(1)^(s+2-q1-q2);
                                elseif i==n
                                    LegMomP_kl=LegMomP_kl+LegMomC(k+1,l+1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*h(2)^(s+2-q1-q2);
                                end
                            end
                        end
                    end
                end
                diff=(LegMomK(k+1,l+1)-LegMomP_kl)^2;
                f=f+diff;
            end
        end

        obj=f;
        
else
    
       f=0;
        for k=0:N
            for l=0:N
                GeoMomP_kl=0;
                for i=1:n
                        for q1=0:k+l+1
                            for q2=0:k+l+2-q1
                                if i<=n-2
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(i+2)^(k+l+2-q1-q2);
                                elseif i==n-1
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(1)^(k+l+2-q1-q2);
                                elseif i==n
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*h(2)^(k+l+2-q1-q2);
                                end
                            end
                        end
                    
                end
                diff=(GeoMomK(k+1,l+1)-GeoMomP_kl)^2;
                f=f+diff;
            end
        end

        obj=f;
        
end

%Define optimization problem
prob = optimproblem('Objective',obj);

%Define constraints
  [a,b,c,d,e,f,g]=ConditionCoeffs(n,Theta);
        Constraints=cell(n,5);
        for i=1:n
            for j=1:5     
                if j==1               
               %K((i-1)*5+1)= h(i)*a(i)+h(i+1)*b(i)+h(i+2)*c(i)>=0;
                       if i<=n-2
                            Constraints{i,1}=a(i)*h(i)+b(i)*h(i+1)+c(i)*h(i+2)>=0;
                       elseif i==n-1
                            Constraints{i,1}=a(i)*h(i)+b(i)*h(i+1)+c(i)*h(1)>=0;
                       elseif i==n
                            Constraints{i,1}=a(i)*h(i)+b(i)*h(1)+c(i)*h(2)>=0;
                       end

                elseif j==2
                       if i<=n-1
                            Constraints{i,j}= d(i)*h(i)+e(i)*h(i+1)>=0;
                       elseif i==n
                            Constraints{i,j}= d(i)*h(i)+e(i)*h(1)>=0;
                       end
                 elseif j==3
                       if i<=n-1
                           Constraints{i,j}=scale-d(i)*h(i)-e(i)*h(i+1)>=0;
                       elseif i==n
                           Constraints{i,j}=scale-d(i)*h(i)-e(i)*h(1)>=0;
                       end   
                 elseif j==4
                       if i<=n-1
                            Constraints{i,j}= f(i)*h(i)+g(i)*h(i+1)>=0;
                       elseif i==n
                            Constraints{i,j}= f(i)*h(i)+g(i)*h(1)>=0;
                       end
                 elseif j==5
                       if i<=n-1
                           Constraints{i,j}=scale-f(i)*h(i)-g(i)*h(i+1)>=0;
                       elseif i==n
                           Constraints{i,j}=scale-f(i)*h(i)-g(i)*h(1)>=0;
                       end
                 end
            end 
        end
        
        
for i=1:n
    for j=1:5
        name=sprintf('constraint%i_%i',i,j);
        String=['prob.Constraints.',name,'=Constraints{i,j}'];
        eval(String);
    end
end        
        
        
end
        
        