function [f,Grad]=ObjectivePlusGradExplicit(h)
global N
global n
global Theta
global LegMomK
global scale 
global LegMomC
%,N,n,Theta,LegMomK,scale define as global variables before call
if nargout>1
    Grad=sparse(n,1);
end

%M=GeometricMomentCoeffs(N,n,Theta);
%LegMomC=LegendreMomentCoeffs(N,n,Theta,M,scale);


        
f=0;
for k=0:N
    for l=0:N
        L=LegMomC(k+1,l+1,:,:,:,:);
        for h1=1:n
                D_LegMomP_kl_h1=0;
                LegMomP_kl=0;
                for i=1:n
                    for s=0:k+l
                        for q1=0:s+1
                            for q2=0:s+2-q1
                                if i<=n-2
                                    LegMomP_kl=LegMomP_kl+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(i+2)^(s+2-q1-q2);
                                    if i==h1&&q1>0
                                            %Compute gradient element
                                            D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(i+2)^(s+2-q1-q2);
                                        
                                    elseif i+1==h1&&q2>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(i+2)^(s+2-q1-q2);
                                    elseif i+2==h1&&(s+2-q1-q2)>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                    end                            
                                elseif i==n-1
                                    LegMomP_kl=LegMomP_kl+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(1)^(s+2-q1-q2);
                                    
                                    if i==h1&&q1>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(1)^(s+2-q1-q2);     
                                    elseif i+1==h1&&q2>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(1)^(s+2-q1-q2);
                                    elseif 1==h1&&(s+2-q1-q2)>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                    end                   
                                elseif i==n
                                    LegMomP_kl=LegMomP_kl+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*h(2)^(s+2-q1-q2);
                                    
                                    if i==h1&&q1>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*h(2)^(s+2-q1-q2);
                                        
                                    elseif 1==h1&&q2>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*h(2)^(s+2-q1-q2);
                                    elseif 2==h1&&(s+2-q1-q2)>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                    end
             
                                    end
                                end
                            end
                        end
                    end
                
                [k,l];
                diff_grad=-2*(LegMomK(k+1,l+1)-LegMomP_kl)*D_LegMomP_kl_h1;
                if nargout > 1
                    Grad(h1)= Grad(h1)+diff_grad;
                end

        end
        diff=(LegMomK(k+1,l+1)-LegMomP_kl)^2;
        f=f+diff;
    end
end

end










