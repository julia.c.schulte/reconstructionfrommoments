function M=GeometricMomentCoeffs(N,n,Theta)


Cos=cos(Theta); 
Sin=sin(Theta);
Cos=[Cos,Cos(2)]; 
Sin=[Sin,Sin(2)];



M=zeros(N+1,N+1,n,2*N+2,2*N+3);
for k=0:N
    for l=0:N
        for i=1:n
            [k,l,i];
            
            
                si=Sin(i);
                si1=Sin(i+1);
                si2=Sin(i+2);
                ci=Cos(i);
                ci1=Cos(i+1);
                ci2=Cos(i+2);
                
                M1=sparse(k+l+1,k+l+1);

                for q1=0:k+l
                    for q2=0:k+l-q1
                        for p=0:k
                            for q=0:l
                                	fact=0;
                                    for m=0:p+q+1
                                        fact=fact+(nchoosek(p+q+1,m)*(-1)^(p+q+1-m))/((p+q+1)*(k+l+2-m));
                                    end
                                for r1=max(0,q1-p):min(q1,q)
                                    for r2=max(0,p+l-q1-q2):min(k+l-q1-q2,l-q)
                                        %[q1,q2,p,q,r1,r2]
                                        M1(q1+1,q2+1)=M1(q1+1,q2+1)+nchoosek(k,p)*nchoosek(l,q)*(ci*si1-si*ci1)^(-p-q-1)*...
                                            (ci1*si2-si1*ci2)^(-k+p-l+q-1)*fact*...
                                            nchoosek(p,q1-r1)*nchoosek(k-p,k+l-q1-q2-r2)*...
                                            nchoosek(q,r1)*nchoosek(l-q,r2)*(-1)^(p-q+q2-k)*...
                                            si^(p-q1+r1)*si1^(k+l-q2-r1-r2)*si2^(q1+q2+r2-l-p)*ci^(q-r1)*ci1^(r1+r2)*ci2^(l-q-r2);
                                    end
                                end
                            end
                        end
                    end
                end

                a=-si1*ci2+ci1*si2;
                b=si*ci2-si2*ci;
                c=-si*ci1+ci*si1;

                M2=sparse(k+l+2,k+l+3); 
                for q1=0:k+l+1
                    for q2=1:k+l-q1+2
                        [q1,q2];
                        if q1~=0
                            M2(q1+1,q2+1)=a*M1(q1,q2);
                        end
                        if q2>=2&& q1<=k+l
                            M2(q1+1,q2+1)=M2(q1+1,q2+1)+b*M1(q1+1,q2-1);
                        end
                        if q1<=k+l && q2>=1&&q2<=k+l-q1+1
                            M2(q1+1,q2+1)=M2(q1+1,q2+1)+c*M1(q1+1,q2);
                        end
                    end
                end

            
            
            M(k+1,l+1,i,1:k+l+2,1:k+l+3) = M2;
        end
    end
end


end