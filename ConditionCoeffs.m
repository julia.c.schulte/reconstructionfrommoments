function [a,b,c,d,e,f,g]=ConditionCoeffs(n,Theta)
Cos=cos(Theta), Sin=sin(Theta);
Cos=[Cos,Cos(2)]; Sin=[Sin,Sin(2)];

a=zeros(1,n);
b=zeros(1,n);
c=zeros(1,n);
d=zeros(1,n);
e=zeros(1,n);
f=zeros(1,n);
g=zeros(1,n);

for i=1:n
    si=Sin(i);
    si1=Sin(i+1);
    si2=Sin(i+2);
    ci=Cos(i);
    ci1=Cos(i+1);
    ci2=Cos(i+2);
    
    
    a(i)=si2*ci1-ci2*si1;
    b(i)=-si2*ci+ci2*si;
    c(i)=si1*ci-ci1*si;
    d(i)=si1/(ci*si1-si*ci1);
    e(i)=-si/(ci*si1-si*ci1);
    f(i)=-ci1/(ci*si1-si*ci1);
    g(i)=ci/(ci*si1-si*ci1);
    
end

end