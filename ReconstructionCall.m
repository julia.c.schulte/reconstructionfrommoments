
Solver=2 %1: Matlab-Problem-based, local optimization, 2: Matlab-solver-based, local optimization ,3: global optimization ,4: 5xlocal solver runs

LegendreVals=1; %0: Geometric moments, 1: Legendre moments
AddNoiseVals=[1];
alpha=3.1; %order alpha means sigma_n^2=1/N^alpha
BodyNrs=1:4; %: 1: square, 2: circle, 3: body of constant width, 4: half circle
Ns=0:5; % maximal order of moments to be reconstructed from
Nmax=max(Ns); 
ns=[4 8 16 32]; % number of normal directions 

global N 
global n
global Theta
global LegMomK
global GeoMomK
global scale

scale=1; %scaling of problem


%Get results-database
try
    Results_RC=readtable('Results_Legendre.csv')
    % Add new column to table
    %     Num=size(Results_RC,1)
    %     Noise=repmat({'0'},Num,1)
    %     T=table(Noise)
    %     Results_RC=[Results_RC,T]
catch
    %Create new database
            N=[];
            n=[];
            Theta=cell(0);   
            BodyNr=[];
            BodyParams=cell(0);   
            sol=cell(0);   
            fval=[];
            exitflag=[];   
            Runtime=[];
            Names={'iterations', 'funcCount', 'constrviolation', 'stepsize', 'firstorderopt', 'cgiterations','algorithm',  'message', 'solver'};
            %output
            for i=1:length(Names)  
                if i<=6
                    eval([Names{i},'=[]']); %iterations, funcCount, constrviolation, stepsize, algorithm, firstorderopt, cgiterations, message, solver
                else
                     eval([Names{i},'=cell(0)']); %iterations, funcCount, constrviolation, stepsize, algorithm, firstorderopt, cgiterations, message, solver
    
                end
            end
            d_N=[];
            SolverVariant=[];
            Noise=cell(0);   
            MomentType=cell(0);   
            Scale=[];
            Results_RC=table(N,n,Theta,BodyNr,BodyParams,sol,fval,exitflag,Runtime,iterations,funcCount,...
                constrviolation, stepsize, algorithm, firstorderopt, cgiterations, message, solver,d_N,SolverVariant,Noise,MomentType,Scale);          
end


%Define shifted and rescaled Legendre polynomials (which are ONB on [0,1])
syms m x

L=@(m,x)sqrt((2*m+1)/scale)*legendreP(m,(2/scale)*x-1);

for Legendre=LegendreVals
        for BodyNr=BodyNrs
             if BodyNr==1 %square
                 %K=[0.25,0.75]x[0.25,0.75]
                syms x
                syms y
                a=scale*[0.25 0.75] ;
                b=scale*[0 0.5];
                BodyParams=[a,b];

                phi=0:0.1:2*pi;
                x_K=[a,a(2),a(1),a(1)];
                y_K=[b(1),b(1),b(2),b(2),b(1)];
                if Legendre
                    LegMomK_true=zeros(Nmax+1,Nmax+1);
                else
                    GeoMomK_true=zeros(Nmax+1,Nmax+1);
                end
                for i=0:Nmax
                    for j=0:Nmax
                        if Legendre
                            LegMomK_true(i+1,j+1)= int(L(i,x),x,a(1),a(2))*int(L(j,y),y,b(1),b(2));  
                        else
                            GeoMomK_true(i+1,j+1)= int(x^i,x,a(1),a(2))*int(y^j,y,b(1),b(2));  
                        end
                    end
                end

            elseif BodyNr==2   

                %K=B((0.5,0.5),0.5) %circle
                syms r
                syms phi

                center=scale*[0.3 0.3];
                radius=scale*0.3;
                BodyParams=[center,radius];
                if Legendre
                    LegMomK_true=zeros(Nmax+1,Nmax+1);
                else
                    GeoMomK_true=zeros(Nmax+1,Nmax+1);
                end
                for i=1:Nmax+1
                    for j=1:Nmax+1
                        if Legendre
                            LegMomK_true(i,j)= int(int(L(i-1,center(1)+r*cos(phi))*L(j-1,center(2)+r*sin(phi))*r,r,0,radius),phi,0,2*pi);
                        else
                            GeoMomK_true(i,j)= int(int((center(1)+r*cos(phi))^(i-1)*(center(2)+r*sin(phi))^(j-1)*r,r,0,radius),phi,0,2*pi);
                        end
                    end
                end
                phi=0:0.1:2*pi;
                x_K=center(1)+radius*cos(phi);
                y_K=center(2)+radius*sin(phi);
                
            elseif BodyNr==3 %Body of constant width
                syms r
                syms phi

                center=scale*[0.5 0.5];
                radius=scale*0.5;

                BodyParams=[center, radius];
                syms m x
                
                if Legendre
                    LegMomK_true=zeros(Nmax+1,Nmax+1);
                else
                    GeoMomK_true=zeros(Nmax+1,Nmax+1);
                end
                %Calculate determinant of parametrization
                %x = @(r,phi)r*(1/10)*(9*cos(phi) +2*cos(2*phi)-cos(4*phi)); 
                %y= @(r,phi)r*(1/10)*(9*sin(phi)-2*sin(2*phi)-sin(4*phi));
                %D=@(r,phi)diff(x(r,phi),r)*diff(y(r,phi),phi)-diff(x(r,phi),phi)*diff(y(r,phi),r)
                %use simplify(expand(ans)) yields -(r*(63*cos(3*phi) + 8*cos(3*phi)^2 - 81))/100;
                
                D=@(phi)-((63*cos(3*phi) + 8*cos(3*phi)^2 - 81))/100;
                
                for i=1:Nmax+1
                    for j=1:Nmax+1
                        if Legendre
                            LegMomK_true(i,j)= int(int(L(i-1,center(1)+r*(1/10)*(9*cos(phi)+2*cos(2*phi)-cos(4*phi)))*L(j-1,center(2)+r*(1/10)*(9*sin(phi)-2*sin(2*phi)-sin(4*phi)))*r*D(phi),r,0,radius),phi,0,2*pi);
               
                            %fun=@(r,phi)L(i-1,center(1)+r.*(1/10).*(9.*cos(phi)+2.*cos(2.*phi)-cos(4.*phi))).*L(j-1,center(2)+r.*(1/10).*(9.*sin(phi)-2.*sin(2*phi)-sin(4*phi))).*r.*D(phi);
                            %LegMomK(i,j)= integral2(fun,0,radius,0,2*pi);
                        else
                            GeoMomK_true(i,j)= int(int((center(1)+r*(1/10)*(9*cos(phi)+2*cos(2*phi)-cos(4*phi)))^(i-1)*(center(2)+r*(1/10)*(9*sin(phi)-2*sin(2*phi)-sin(4*phi)))^(j-1)*r*D(phi),r,0,radius),phi,0,2*pi);
   
                            %fun=@(r,phi)(center(1)+r.*(1/10).*(9*cos(phi)+2*cos(2*phi)-cos(4*phi))).^(i-1).*(center(2)+r.*(1/10).*(9*sin(phi)-2*sin(2*phi)-sin(4*phi))).^(j-1).*r.*D(phi);
                            %GeoMomK(i,j)= integral2(fun,0,radius,0,2*pi);
                        end
                    end
                end

                phi=0:0.1:2*pi;
                x_K =center(1)+radius*(1/10)*(9*cos(phi) +2*cos(2*phi)-cos(4*phi)); 
                y_K=center(2)+radius*(1/10)*(9*sin(phi)-2*sin(2*phi)-sin(4*phi));
             elseif BodyNr==4
                % K=halfcircle B((0.3,0.3),0.3)\cap {(x,y):y>=0.3} 
                syms r
                syms phi

                center=scale*[0.3 0.3];
                radius=scale*0.3;
                BodyParams=[center,radius];
                if Legendre
                    LegMomK_true=zeros(Nmax+1,Nmax+1);
                else
                    GeoMomK_true=zeros(Nmax+1,Nmax+1);
                end
                for i=1:Nmax+1
                    for j=1:Nmax+1
                        if Legendre
                            LegMomK_true(i,j)= int(int(L(i-1,center(1)+r*cos(phi))*L(j-1,center(2)+r*sin(phi))*r,r,0,radius),phi,0,pi);
                        else
                            GeoMomK_true(i,j)= int(int((center(1)+r*cos(phi))^(i-1)*(center(2)+r*sin(phi))^(j-1)*r,r,0,radius),phi,0,pi);
                        end
                    end
                end
                phi=0:0.1:pi;
                x_K=center(1)+radius*cos(phi);
                y_K=center(2)+radius*sin(phi);
             end
         
        for AddNoise=AddNoiseVals
            for N=Ns    
                
               
        
                for n=ns
                    [BodyNr,N,n]
                    
                     %Calculate and add noise
                    Noise=zeros(Nmax+1,Nmax+1);
                    if AddNoise
                        for i=0:Nmax
                            for j=0:Nmax
                                Noise(i+1,j+1)=  randn(1)*0.01*min(1,(1/sqrt(N^alpha)));
                            end
                        end  
                    end
                    if Legendre
                        LegMomK=LegMomK_true+Noise;
                    else
                        GeoMomK=GeoMomK_true+Noise;
                    end  

                    %Reconstruction Angles
                    Theta=linspace(0,2*pi,n+1);
                    
                    %specify Solver options
                    
                    if Solver==1 % Matlab, problem-based, local solver
                        options = optimoptions('fmincon','MaxFunctionEvaluations',3000,'StepTolerance',10^(-4));
                    else
                        if Legendre
                            options=optimoptions(@fmincon,'Algorithm','interior-point','Display','iter','CheckGradients',false,'SpecifyObjectiveGradient',true,'HessianFcn',@ObjectiveHessExplicit,...
                                'MaxFunctionEvaluations',100,'StepTolerance',10^(-5),'UseParallel',true);
                        else
                            options=optimoptions(@fmincon,'Algorithm','interior-point','Display','iter','CheckGradients',false,'SpecifyObjectiveGradient',true,'HessianFcn',@ObjectiveHessExplicit_GeoMom,...
                                'MaxFunctionEvaluations',100,'StepTolerance',10^(-5),'UseParallel',true);
                        end
                    end
                    
                    %Call reconstruction
                    if Legendre
                        Result=ReconstructFromLegMom(LegMomK(1:N+1,1:N+1),Solver,options);
                    else
                        Result=ReconstructFromGeoMom(GeoMomK(1:N+1,1:N+1),Solver,options);
                    end
                    
                   
                    h=Result.sol;
                    Results_RC.fval(end+1,1)=Result.fval;
                    Results_RC.sol{end,1}=num2str(h)
                    Results_RC.exitflag(end,1)=string(Result.exitflag);

                    Result.output.message=regexprep(Result.output.message,'\s+',' ');
                    Names={'iterations', 'funcCount', 'constrviolation', 'stepsize',  'firstorderopt', 'cgiterations','algorithm', 'message', 'solver'};
                    %output
                    for i=1:length(fields(Result.output))  
                        try
                            if i<=6
                                eval(['Results_RC.',Names{i},'(end,1)=Result.output.',Names{i},';']); %iterations, funcCount, constrviolation, stepsize, algorithm, firstorderopt, cgiterations, message, solver
                            else
                                eval(['Results_RC.',Names{i},'{end,1}=Result.output.',Names{i},';']); %iterations, funcCount, constrviolation, stepsize, algorithm, firstorderopt, cgiterations, message, solver
                            end
                        catch
                        end

                   end
                    Results_RC.Runtime(end,1)=Result.Runtime;
                    Results_RC.N(end,1)=Result.N;
                    Results_RC.n(end,1)=Result.n;
                    Results_RC.Theta{end,1}=num2str(Result.Theta);   
                    Results_RC.BodyNr(end,1)=BodyNr;
                    Results_RC.BodyParams{end,1}=num2str(BodyParams/scale);
                    Results_RC.Noise{end,1}=mat2str(Noise);
                    Results_RC.SolverVariant(end,1)=Solver;

                    %Calculation of Nikodym-distance to true K
                    K=polyshape(x_K,y_K);
                    V=PlotPolygon(n,h,Theta,0);
                    P=polyshape(V(1,:),V(2,:));
                    KintP=intersect(K,P);
                    A_KintP=area(KintP);
                    d_N=area(K)+area(P)-2*area(KintP);
                    Results_RC.d_N(end,1)=d_N;
                    if Legendre
                        Results_RC.MomentType{end,1}='Legendre';
                    else
                        Results_RC.MomentType{end,1}='Geometric';
                    end

                    Results_RC.Scale(end,1)=scale;
                    writetable(Results_RC,'Results_Legendre.csv');

                end
            end
        end
    end
end

