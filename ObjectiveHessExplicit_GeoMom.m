function Hess=ObjectiveHessExplicit_GeoMom(h,lambda)

global N 
global n
global Theta
global GeoMomK
global scale
global M
%make N,n,Theta,GeoMomK,scale global before function call 
Hessf=sparse(n,n);

%M=GeometricMomentCoeffs(N,n,Theta);
%LegMomC=LegendreMomentCoeffs(N,n,Theta,M,scale);


        
f=0;
for k=0:N
    for l=0:N
        for h1=1:n
            for h2=1:n
                D_GeoMomP_kl_h1=0;
                D_GeoMomP_kl_h2=0;
                D_GeoMomP_kl_h1h2=0;
                GeoMomP_kl=0;
                for i=1:n
                        for q1=0:k+l+1
                            for q2=0:k+l+2-q1                                    
                                if i==n-1
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(1)^(k+l+2-q1-q2);
                                    
                                     if i==h1
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(1)^(k+l+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*(q1-1)*h(i)^(q1-2)*h(i+1)^q2*h(1)^(k+l+2-q1-q2);
                                        elseif i+1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(1)^(k+l+2-q1-q2);
                                        elseif 1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(k+l+2-q1-q2)*h(1)^(k+l+2-q1-q2-1);
                                        end
                                        
                                    elseif i+1==h1
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(1)^(k+l+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(1)^(k+l+2-q1-q2);
                                        elseif i+1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*(q2-1)*h(i+1)^(q2-2)*h(1)^(k+l+2-q1-q2);
                                        elseif 1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(k+l+2-q1-q2)*h(1)^(k+l+2-q1-q2-1);
                                        end
                                    elseif 1==h1
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+l+2-q1-q2)*h(1)^(k+l+2-q1-q2-1);
                                        %Compute Hessian element
                                        if i==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(k+l+2-q1-q2)*h(1)^(k+l+2-q1-q2-1);
                                        elseif i+1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(k+l+2-q1-q2)*h(1)^(k+l+2-q1-q2-1);
                                        elseif 1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+l+2-q1-q2)*(k+l+2-q1-q2-1)*h(1)^(k+l+2-q1-q2-2);
                                        end
                                    end
                                    
                                    %Compute gradient element for h2
                                    if i==h2
                                        D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(1)^(k+l+2-q1-q2);
                                    elseif i+1==h2
                                        D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(1)^(k+l+2-q1-q2);
                                    elseif 1==h2
                                        D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+l+2-q1-q2)*h(1)^(k+l+2-q1-q2-1);
                                    end                     
                                elseif i==n
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*h(2)^(k+l+2-q1-q2);
                                    
                                        if i==h1
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*h(2)^(k+l+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*(q1-1)*h(i)^(q1-2)*h(1)^q2*h(2)^(k+l+2-q1-q2);
                                        elseif 1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(1)^(q2-1)*h(2)^(k+l+2-q1-q2);
                                        elseif 2==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*(k+l+2-q1-q2)*h(2)^(k+l+2-q1-q2-1);
                                        end
                                        
                                    elseif 1==h1
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*h(2)^(k+l+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(1)^(q2-1)*h(2)^(k+l+2-q1-q2);
                                        elseif 1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*(q2-1)*h(1)^(q2-2)*h(2)^(k+l+2-q1-q2);
                                        elseif 2==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*(k+l+2-q1-q2)*h(2)^(k+l+2-q1-q2-1);
                                        end
                                    elseif 2==h1
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*(k+l+2-q1-q2)*h(2)^(k+l+2-q1-q2-1);
                                        %Compute Hessian element
                                        if i==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*(k+l+2-q1-q2)*h(2)^(k+l+2-q1-q2-1);
                                        elseif 1==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*(k+l+2-q1-q2)*h(2)^(k+l+2-q1-q2-1);
                                        elseif 2==h2
                                            D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*(k+l+2-q1-q2)*(k+l+2-q1-q2-1)*h(2)^(k+l+2-q1-q2-2);
                                        end
                                    end
                                    
                                    %Compute gradient element for h2
                                    if i==h2
                                        D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*h(2)^(k+l+2-q1-q2);
                                    elseif 1==h2
                                        D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*h(2)^(k+l+2-q1-q2);
                                    elseif 2==h2
                                        D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*(k+l+2-q1-q2)*h(2)^(k+l+2-q1-q2-1);
                                    end
                                else
                                        GeoMomP_kl=GeoMomP_kl+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(i+2)^(k+l+2-q1-q2);
                                        if i==h1
                                                %Compute gradient element
                                                D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(i+2)^(k+l+2-q1-q2);

                                                %Compute Hessian element
                                                if i==h2
                                                    D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*(q1-1)*h(i)^(q1-2)*h(i+1)^q2*h(i+2)^(k+l+2-q1-q2);
                                                elseif i+1==h2
                                                    D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(i+2)^(k+l+2-q1-q2);
                                                elseif i+2==h2
                                                    D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(k+l+2-q1-q2)*h(i+2)^(k+l+2-q1-q2-1);
                                                end

                                        elseif i+1==h1
                                            %Compute gradient element
                                            D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(i+2)^(k+l+2-q1-q2);

                                            %Compute Hessian element
                                            if i==h2
                                                D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(i+2)^(k+l+2-q1-q2);
                                            elseif i+1==h2
                                                D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*(q2-1)*h(i+1)^(q2-2)*h(i+2)^(k+l+2-q1-q2);
                                            elseif i+2==h2
                                                D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(k+l+2-q1-q2)*h(i+2)^(k+l+2-q1-q2-1);
                                            end
                                        elseif i+2==h1
                                            %Compute gradient element
                                            D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+l+2-q1-q2)*h(i+2)^(k+l+2-q1-q2-1);
                                            %Compute Hessian element
                                            if i==h2
                                                D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(k+l+2-q1-q2)*h(i+2)^(k+l+2-q1-q2-1);
                                            elseif i+1==h2
                                                D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(k+l+2-q1-q2)*h(i+2)^(k+l+2-q1-q2-1);
                                            elseif i+2==h2
                                                D_GeoMomP_kl_h1h2=D_GeoMomP_kl_h1h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+l+2-q1-q2)*(k+l+2-q1-q2-1)*h(i+2)^(k+l+2-q1-q2-2);
                                            end
                                        end

                                        %Compute gradient element for h2
                                        if i==h2
                                            D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(i+2)^(k+l+2-q1-q2);
                                        elseif i+1==h2
                                            D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(i+2)^(k+l+2-q1-q2);
                                        elseif i+2==h2
                                            D_GeoMomP_kl_h2=D_GeoMomP_kl_h2+M(k+1,l+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+l+2-q1-q2)*h(i+2)^(k+l+2-q1-q2-1);
                                        end
                                   end
                                end
                            end
                        end
                    
                
                [k,l];


                diff_Hess=-2*(-D_GeoMomP_kl_h2*D_GeoMomP_kl_h1+(GeoMomK(k+1,l+1)-GeoMomP_kl)*D_GeoMomP_kl_h1h2);
                Hessf(h1,h2)= Hessf(h1,h2)+diff_Hess;
                

            end
        end
        diff=(GeoMomK(k+1,l+1)-GeoMomP_kl)^2;
        f=f+diff;
    end
end


Hess=sparse(Hessf);


end










