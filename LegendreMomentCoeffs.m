function L=LegendreMomentCoeffs(N,n,Theta,M,scale)

C=LegendreCoefficientMatrix(N,scale);
%M=GeometricMomentCoeffs(N,n,Theta);

L=zeros(N+1,N+1,n,2*N+2,2*N+3,2*N+4);
for k=0:N
    for l=0:N
        for i=1:n
            for s=0:k+l
                [k,l,i,s];
                Ltilde=sparse(k+l+2,k+l+3);
                
                for q=max(s-k,0):min(s,l)
                    Ltilde=Ltilde+C(k+1,s-q+1)*C(l+1,q+1)*reshape(M([s-q+1],[q+1],[i],[1:k+l+2],[1:k+l+3]),k+l+2,k+l+3);
                end
                
                    L(k+1,l+1,i,s+1,1:k+l+2,1:k+l+3) =Ltilde;
            end
        end
    end
end

end