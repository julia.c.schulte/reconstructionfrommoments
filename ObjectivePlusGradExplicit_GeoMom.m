function [f,Grad]=ObjectivePlusGradExplicit_GeoMom(h)
global N
global n
global Theta
global GeoMomK
global scale
global M
%,N,n,Theta,LegMomK,scale define as global variables before call
if nargout>1
    Grad=zeros(n,1);
end

%M=GeometricMomentCoeffs(N,n,Theta);
%LegMomC=LegendreMomentCoeffs(N,n,Theta,M,scale);


        
f=0;
for k=0:N
    for el=0:N
        for h1=1:n
                D_GeoMomP_kl_h1=0;
                GeoMomP_kl=0;
                for i=1:n
                        for q1=0:k+el+1
                            for q2=0:k+el+2-q1
                                if i<=n-2
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(i+2)^(k+el+2-q1-q2);
                                    if i==h1&&q1>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(i+2)^(k+el+2-q1-q2);                                       
                                    elseif i+1==h1&&q2>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(i+2)^(k+el+2-q1-q2);
                                    elseif i+2==h1&&(k+el+2-q1-q2)>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+el+2-q1-q2)*h(i+2)^(k+el+2-q1-q2-1);
                                    end                            
                                elseif i==n-1
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(1)^(k+el+2-q1-q2);
                                    if i==h1&&q1>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(1)^(k+el+2-q1-q2);
                                    elseif i+1==h1&&q2>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(1)^(k+el+2-q1-q2);
                                    elseif 1==h1&&(k+el+2-q1-q2)>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(k+el+2-q1-q2)*h(1)^(k+el+2-q1-q2-1);
                                    end                   
                                elseif i==n
                                    GeoMomP_kl=GeoMomP_kl+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*h(2)^(k+el+2-q1-q2);
                                    
                                    if i==h1&&q1>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*h(2)^(k+el+2-q1-q2);
                                        
                                    elseif 1==h1&&q2>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*h(2)^(k+el+2-q1-q2);
                                    elseif 2==h1&&(k+el+2-q1-q2)>0
                                        %Compute gradient element
                                        D_GeoMomP_kl_h1=D_GeoMomP_kl_h1+M(k+1,el+1,i,q1+1,q2+1)*h(i)^q1*h(1)^q2*(k+el+2-q1-q2)*h(2)^(k+el+2-q1-q2-1);
                                    end
                                end
                           end
                       end
                end
                
                [k,el];
                diff_grad=-2*(GeoMomK(k+1,el+1)-GeoMomP_kl)*D_GeoMomP_kl_h1;
                if nargout > 1
                    Grad(h1)= Grad(h1)+diff_grad;
                end

        end
        diff=(GeoMomK(k+1,el+1)-GeoMomP_kl)^2;
        f=f+diff;
        end
    end
end











