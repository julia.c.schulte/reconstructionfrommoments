function Result=ReconstructFromGeoMom(LegMomK,Solver,options)

    tic    

    global N 
    global n
    global Theta
    global LegMomK
    global scale
    global M
    global GeoMomK
    global LegMomC
    M=GeometricMomentCoeffs(N,n,Theta);


        
    if Solver==1 %Matlab-Problem based approach
        
        h=optimvar('h',1,n);
        prob=DefineProb(N,n,Theta,scale,0)

        show(prob)
        x0.h = rand(1,n);

        [sol,fval,exitflag,output] = solve(prob,x0,'Options',options)   
        Runtime=toc;
        sol=sol.h;
        
    elseif Solver==2 %Matlab Solver-based local approach
         
        %Write Constraints
            [A,b]=ConstraintMatrix(n,Theta,scale); %Creates constraint-function-m-file constr.m
            A=sparse(A);
            
        %Create Problem Structure
            h0=0.5*(1+cos(Theta(1:end-1))+sin(Theta(1:end-1)));
            %h0=rand(n,1)*scale;
            h_low=-2*ones(n,1)*scale;
            h_up=2*ones(n,1)*scale;
             
            problem = createOptimProblem('fmincon',...
                'objective',@(h)ObjectivePlusGradExplicit_GeoMom(h),...
                'x0',h0,'Aineq',A,'bineq',b,'lb',h_low,'ub',h_up,'options',options);
            
        %Run local solver
            [sol,fval,exitflag,output] = fmincon(problem)
            %clear symbolic variable h
            syms h
            Runtime=toc;
       
    elseif Solver==3 %Matlab Solver-based global approach
        %Write Constraints
            [A,b]=ConstraintMatrix(n,Theta,scale); %Creates constraint-function-m-file constr.m
            A=sparse(A);
            
        %Create Problem Structure
            
            
            WindowSupValues=[cos(Theta);sin(Theta);cos(Theta)+sin(Theta);zeros(1,n+1)];
            h_low=min(WindowSupValues(:,1:end-1))*scale;
            h_up=max(WindowSupValues(:,1:end-1))*scale;
            h0=h_low'+rand(n,1).*(h_up-h_low)';
%             h_low=-2*ones(n,1)*scale;
%             h_up=2*ones(n,1)*scale;
%             h0=rand(n,1)*scale; 
            
            problem = createOptimProblem('fmincon',...
                'objective',@(h)ObjectivePlusGradExplicit_GeoMom(h),...
                'x0',h0,'Aineq',A,'bineq',b,'lb',h_low,'ub',h_up,'options',options);

        %Create global search problem
            problem.options.Display = 'iter';
            gs = GlobalSearch('Display','final','FunctionTolerance',10^(-4),'StartPointsToRun','bounds-ineqs');
            gs.NumTrialPoints=1000;
        %Run global solver
            %rng(14,'twister') % for reproducibility
            [sol,fval,exitflag,output,solutions] = run(gs,problem);
            try
                output=solutions(1).Output;
            end
            sol=sol';
            Runtime=toc;
    elseif Solver==4 % Best of 5 local solver runs
         %Write Constraints
            h=optimvar('h',1,n);
            [A,b]=ConstraintMatrix(n,Theta,scale); %Creates constraint-function-m-file constr.m
            A=sparse(A);
         
        for k=1:5  %Call local solver 5 times
        %Create Problem Structure
            h0=rand(n,1)*scale;
            h_low=-2*ones(n,1)*scale;
            h_up=2*ones(n,1)*scale;
             
            problem = createOptimProblem('fmincon',...
                'objective',@(h)ObjectivePlusGradExplicit_GeoMom(h),...
                'x0',h0,'Aineq',A,'bineq',b,'lb',h_low,'ub',h_up,'options',options);
       
        %Run local solver
           [sol_k,fval_k,exitflag_k,output_k] = fmincon(problem)
           
            if k==1||fval_k<=fval
                sol=sol_k;
                fval=fval_k;
                exitflag=exitflag_k;
                output=output_k;
            end
            sol=sol';
            if fval<=10^(-2)
                break;
            end
        end
            Runtime=toc;   

    elseif Solver==5 %SparsePOP
    elseif Solver==6 %Gloptipoly
    end
    
    Result=[];
    Result.sol=sol/scale;
    Result.fval=fval;
    Result.exitflag=exitflag;
    Result.output=output;
    Result.Runtime=Runtime;
    Result.SolverVariant=Solver;
    Result.N=N;
    Result.n=n;
    Result.Theta=Theta;   

end