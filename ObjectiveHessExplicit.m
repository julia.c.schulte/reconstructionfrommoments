function Hess=ObjectiveHessExplicit(h,lambda)

global N 
global n
global Theta
global LegMomK
global scale
global LegMomC
%make N,n,Theta,LegMomK,scale global before function call 
Gradf=sparse(n,1);
Hessf=sparse(n,n);

%M=GeometricMomentCoeffs(N,n,Theta);
%LegMomC=LegendreMomentCoeffs(N,n,Theta,M,scale);


        
f=0;
for k=0:N
    for l=0:N
        L=LegMomC(k+1,l+1,:,:,:,:);
        for h1=1:n
            for h2=1:n
                D_LegMomP_kl_h1=0;
                D_LegMomP_kl_h2=0;
                D_LegMomP_kl_h1h2=0;
                LegMomP_kl=0;
                for i=1:n
                    for s=0:k+l
                        for q1=0:s+1
                            for q2=0:s+2-q1                                    
                                if i==n-1
                                    LegMomP_kl=LegMomP_kl+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(1)^(s+2-q1-q2);
                                    
                                        if i==h1
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(1)^(s+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*(q1-1)*h(i)^(q1-2)*h(i+1)^q2*h(1)^(s+2-q1-q2);
                                        elseif i+1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(1)^(s+2-q1-q2);
                                        elseif 1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                        end
                                        
                                    elseif i+1==h1
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(1)^(s+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(1)^(s+2-q1-q2);
                                        elseif i+1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*(q2-1)*h(i+1)^(q2-2)*h(1)^(s+2-q1-q2);
                                        elseif 1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                        end
                                    elseif 1==h1
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                        %Compute Hessian element
                                        if i==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                        elseif i+1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                        elseif 1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*(s+2-q1-q2-1)*h(1)^(s+2-q1-q2-2);
                                        end
                                    end
                                    
                                    %Compute gradient element for h2
                                    if i==h2
                                        D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(1)^(s+2-q1-q2);
                                    elseif i+1==h2
                                        D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(1)^(s+2-q1-q2);
                                    elseif 1==h2
                                        D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*h(1)^(s+2-q1-q2-1);
                                    end                     
                                elseif i==n
                                    LegMomP_kl=LegMomP_kl+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*h(2)^(s+2-q1-q2);
                                    
                                                                    if i==h1&&q1>0
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*h(2)^(s+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*(q1-1)*h(i)^(q1-2)*h(1)^q2*h(2)^(s+2-q1-q2);
                                        elseif 1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(1)^(q2-1)*h(2)^(s+2-q1-q2);
                                        elseif 2==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                        end
                                        
                                    elseif 1==h1
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*h(2)^(s+2-q1-q2);
                                        
                                        %Compute Hessian element
                                        if i==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(1)^(q2-1)*h(2)^(s+2-q1-q2);
                                        elseif 1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*(q2-1)*h(1)^(q2-2)*h(2)^(s+2-q1-q2);
                                        elseif 2==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                        end
                                    elseif 2==h1
                                        %Compute gradient element
                                        D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                        %Compute Hessian element
                                        if i==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                        elseif 1==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                        elseif 2==h2
                                            D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*(s+2-q1-q2)*(s+2-q1-q2-1)*h(2)^(s+2-q1-q2-2);
                                        end
                                    end
                                    
                                    %Compute gradient element for h2
                                    if i==h2
                                        D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(1)^q2*h(2)^(s+2-q1-q2);
                                    elseif 1==h2
                                        D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(1)^(q2-1)*h(2)^(s+2-q1-q2);
                                    elseif 2==h2
                                        D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(1)^q2*(s+2-q1-q2)*h(2)^(s+2-q1-q2-1);
                                    end
                                else
                                        LegMomP_kl=LegMomP_kl+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*h(i+2)^(s+2-q1-q2);
                                        if i==h1
                                                %Compute gradient element
                                                D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(i+2)^(s+2-q1-q2);

                                                %Compute Hessian element
                                                if i==h2
                                                    D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*(q1-1)*h(i)^(q1-2)*h(i+1)^q2*h(i+2)^(s+2-q1-q2);
                                                elseif i+1==h2
                                                    D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(i+2)^(s+2-q1-q2);
                                                elseif i+2==h2
                                                    D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                                end

                                        elseif i+1==h1
                                            %Compute gradient element
                                            D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(i+2)^(s+2-q1-q2);

                                            %Compute Hessian element
                                            if i==h2
                                                D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*q2*h(i+1)^(q2-1)*h(i+2)^(s+2-q1-q2);
                                            elseif i+1==h2
                                                D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*(q2-1)*h(i+1)^(q2-2)*h(i+2)^(s+2-q1-q2);
                                            elseif i+2==h2
                                                D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                            end
                                        elseif i+2==h1
                                            %Compute gradient element
                                            D_LegMomP_kl_h1=D_LegMomP_kl_h1+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                            %Compute Hessian element
                                            if i==h2
                                                D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                            elseif i+1==h2
                                                D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                            elseif i+2==h2
                                                D_LegMomP_kl_h1h2=D_LegMomP_kl_h1h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*(s+2-q1-q2-1)*h(i+2)^(s+2-q1-q2-2);
                                            end
                                        end

                                        %Compute gradient element for h2
                                        if i==h2
                                            D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*q1*h(i)^(q1-1)*h(i+1)^q2*h(i+2)^(s+2-q1-q2);
                                        elseif i+1==h2
                                            D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*q2*h(i+1)^(q2-1)*h(i+2)^(s+2-q1-q2);
                                        elseif i+2==h2
                                            D_LegMomP_kl_h2=D_LegMomP_kl_h2+L(1,1,i,s+1,q1+1,q2+1)*h(i)^q1*h(i+1)^q2*(s+2-q1-q2)*h(i+2)^(s+2-q1-q2-1);
                                        end
                                   end
                                end
                            end
                        end
                    end
                
                [k,l];


                diff_Hess=-2*(-D_LegMomP_kl_h2*D_LegMomP_kl_h1+(LegMomK(k+1,l+1)-LegMomP_kl)*D_LegMomP_kl_h1h2);
                Hessf(h1,h2)= Hessf(h1,h2)+diff_Hess;
                

            end
                diff_grad=-2*(LegMomK(k+1,l+1)-LegMomP_kl)*D_LegMomP_kl_h1;
                Gradf(h1)= Gradf(h1)+diff_grad;
        end
        diff=(LegMomK(k+1,l+1)-LegMomP_kl)^2;
        f=f+diff;
    end
end


Hess=sparse(Hessf);


end










