# ReconstructionFromMoments

This project contains the Matlab-files for reconstructing planar convex bodies from geometric and Legendre moments. 
The description of the algorithm, reconstruction examples as well as uniqueness, stability and consistency 
results are contained in the article
Julia Schulte, Astrid Kousholt: Reconstruction of convex bodies from moments, https://arxiv.org/abs/1605.06362


ReconstructionCall.m: 
	Calculates the Legendre moments for three different bodies and calls the reconstruction algorithm in a 
	loop over different regimes of parameters n,N,Theta. Saves results in a csv-file "Results_Legendre.csv".
	
ReconstructFromGeoMom.m:
	Reconstructs the convex body from geometric moments
	
ReconstructFromLegMom.m:
	Reconstructs the convex body from Legendre moments

GeometricMomentCoeffs.m:
	Calculates the coefficients of the geometric moments of the polygon P(h) as multivariate polynomial of h

LegendreMomentCoeffs.m
	Calculates the coefficients of the Legendre moments of the polygon P(h) as multivariate polynomial of h

LegendreCoefficientMatrix.m
	Calculates the coefficient matrix for the Legendre polynomials

ConstraintMatrix.m:
	Calculates a matrix A and vector b with the parameters of the linear constraints

ObjectivePlusGradExplicit.m
	Calculates the objective function and its gradient for the reconstruction from Legendre moments

ObjectiveHessExplicit.m:
	Calculates the Hessian of the objective function for the reconstruction from Legendre moments

ObjectivePlusGradExplicit_GeoMom.m: 
	Calculates the objective function and its gradient for the reconstruction from geometric moments

ObjectiveHessExplicit_GeoMom.m:
	Calculates the Hessian of the objective function for the reconstruction from geometric moments

PlotPolygon.m: 
	Plots a polygon given the support function values h and the outer normal directions Theta

EvaluationScript.m
	Evaluates the results saved in the file "Results_Legendre.csv"

EvaluationScript2.m
	Evaluates the results saved in the file "Results_Legendre.csv" and makes figures with 6 plots each