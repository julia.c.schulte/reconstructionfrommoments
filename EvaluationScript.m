 Results= readtable('Results_Legendre.csv') % Results= readtable('Results_Geometric.csv') 
 Num=size(Results,1);
 NewDirName=['figures/Runs/Run_',char(datetime('now','Format','y-MMM-d_HH-mm-ss'))];
mkdir(NewDirName);
 
 for i=1:Num
    Result=Results(i,:);
    BodyNr=Result.BodyNr;
    N=Result.N;
    n=Result.n;
    Noise=eval(Result.Noise{1});
    eval(sprintf(['Theta=[',Result.Theta{1},']']))
    eval(sprintf(['h=[',Result.sol{1},']']))
    normals=0;
    figure(1)
    clf
    V=PlotPolygon(n,h,Theta,normals);  
    hold on
    eval(sprintf(['BodyParams=[',Result.BodyParams{1},']']));
    if BodyNr==1
        a=BodyParams(1:2);
        b=BodyParams(3:4);
        x_K=[a,a(2),a(1),a(1)];
        y_K=[b(1),b(1),b(2),b(2),b(1)];
    elseif BodyNr==2
        center=BodyParams(1:2);
        radius=BodyParams(3);
        phi=0:0.01:2*pi;
        x_K=center(1)+radius*cos(phi);
        y_K=center(2)+radius*sin(phi);
    elseif BodyNr==3
        center=BodyParams(1:2);
        radius=BodyParams(3);
        phi=0:0.1:2*pi;
        x_K =center(1)+radius*(1/10)*(9*cos(phi) +2*cos(2*phi)-cos(4*phi)); 
        y_K=center(2)+radius*(1/10)*(9*sin(phi)-2*sin(2*phi)-sin(4*phi));
    elseif BodyNr==4
        %Example 1: K1=halfcircle B((0.3,0.3),0.3)\cap {(x,y):y>=0.3} %sphere
        syms r
        syms phi

        %matrix of moments of K1=B((0.5,0.5),0.5)
        center=BodyParams(1:2);%[0.3 0.3];
        radius=BodyParams(3);%0.3;
        BodyParams=[center,radius];
        phi=[0:0.1:pi,pi,0];
        x_K=center(1)+radius*cos(phi);
        y_K=center(2)+radius*sin(phi);
    end
    plot(x_K,y_K,'k-')
    legend(sprintf('N=%d and n=%d',N,n),'True body');
    
    ax = gca;
    axis equal
    xlim([0,1.1]);
    ylim([0,1.1]);
    if BodyNr==3
        xlim([0,1.4]);
        ylim([0,1.4]);  
    end
    xticks(0:0.2:1.3);
    yticks(0:0.2:1.3);

            

    ax.FontSize=18;
  
    outerpos = ax.OuterPosition;
    
    ti = ax.TightInset; 
    left = outerpos(1) + ti(1);
    bottom = outerpos(2) + ti(2);
    ax_width = outerpos(3) - ti(1) - ti(3);
    ax_height = outerpos(4) - ti(2) - ti(4);
    ax.Position = [left bottom ax_width ax_height];
    
    if strcmp(Results.MomentType,'Legendre')  
        if max(max(Noise))>0
            Name=sprintf('LegendreNoise1_BodyNr%d_N%d_n%d%BodyNr',BodyNr,N,n);
        else
            Name=sprintf('LegendreNoise0_BodyNr%d_N%d_n%d%BodyNr',BodyNr,N,n);
        end
    else
        if max(max(Noise))>0
            Name=sprintf('GeometricNoise1_BodyNr%d_N%d_n%d%BodyNr',BodyNr,N,n);
        else
            Name=sprintf('GeometricNoise0_BodyNr%d_N%d_n%d%BodyNr',BodyNr,N,n);
        end
    end
    print(gcf,'-dpng',[NewDirName,'/',Name,'.png']);
    
     %Calculation of Nikodym-distance
     K=polyshape(x_K,y_K);    
     P=polyshape(V(1,:),V(2,:));
     KintP=intersect(K,P);
     A_KintP=area(KintP);
     i
     d_N=area(K)+area(P)-2*area(KintP);

 end
 
  %Number of Terms
    NumTerms=0;
    for k=0:N
        for l=0:N
            NumTerms=NumTerms+1;
            Num2=0;
            for i=1:n
                for s=0:k+l
                    for q1=0:s+1
                        Num2=Num2+(s+2-q1+1);
                    end
                end
            end
            NumTerms=NumTerms+Num2+Num2^2;
        end
    end
 %#terms cost fct.: 
 %#constraints: 5*n
 

     