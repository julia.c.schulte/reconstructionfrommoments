Results= readtable('Results_Legendre.csv'); %Results= readtable('Results_Geometric.csv');
Num=size(Results,1);
NewDirName=['figures/Runs/Run_',char(datetime('now','Format','y-MMM-d_HH-mm-ss'))];
mkdir(NewDirName);

% make subplots with N=1,3,5 and n=16, 32. (use table operations tf = (T.Smoker == false);)

for BodyNr=1:4
    idx1=0;
    idx2=0;
    figure(1)
    clf
    figure(2)
    clf
    for n=[8 32] 
        for N=[1 3 5]       
   
            Results_sel=Results(logical((Results.BodyNr==BodyNr).*(Results.N==N).*(Results.n==n)),:);
            Num=size(Results_sel,1);
            for k=1:Num
                Result=Results_sel(k,:);
                Noise=eval(Result.Noise{1});
        if max(max(Noise))>0
            AddNoise=1;
            idx1=idx1+1
            
            figure(2)
            subplot(2,3,idx1);
            hold on
        else
            AddNoise=0;
            idx2=idx2+1
            
            figure(1)
            subplot(2,3,idx2);
            hold on
        end
            eval(sprintf(['Theta=[',Result.Theta{1},']']))
            eval(sprintf(['h=[',Result.sol{1},']']))
            normals=0;
            V=PlotPolygon(n,h,Theta,normals);
            
            eval(sprintf(['BodyParams=[',Result.BodyParams{1},']']));
           
            %Plot true body
            if BodyNr==1
                a=BodyParams(1:2);
                b=BodyParams(3:4);
                x_K=[a,a(2),a(1),a(1)];
                y_K=[b(1),b(1),b(2),b(2),b(1)];
            elseif BodyNr==2
                center=BodyParams(1:2);
                radius=BodyParams(3);
                phi=0:0.01:2*pi;
                x_K=center(1)+radius*cos(phi);
                y_K=center(2)+radius*sin(phi);
            elseif BodyNr==3
                center=BodyParams(1:2);
                radius=BodyParams(3);
                phi=0:0.1:2*pi;
                x_K =center(1)+radius*(1/10)*(9*cos(phi) +2*cos(2*phi)-cos(4*phi)); 
                y_K=center(2)+radius*(1/10)*(9*sin(phi)-2*sin(2*phi)-sin(4*phi));
            elseif BodyNr==4
                syms r
                syms phi
                
                center=BodyParams(1:2);%[0.3 0.3];
                radius=BodyParams(3);%0.3;
                phi=[0:0.1:pi,pi,0];
                x_K=center(1)+radius*cos(phi);
                y_K=center(2)+radius*sin(phi);
            end
            hold on
            plot(x_K,y_K,'k-')
            legend(sprintf('N=%d and n=%d',N,n),'True body');

            %Format axis
            ax = gca;
            axis equal
            xlim([0,1.1]);
            ylim([0,1.1]);
            if BodyNr==3
                xlim([0,1.4]);
                ylim([0,1.4]);  
            end
            xticks(0:0.2:1.3);
            yticks(0:0.2:1.3);
            ax.FontSize=16;
            end
        
        end
    end

    if strcmp(Result.MomentType,'Legendre')  
        figure(2)
        Name=sprintf('LegendreNoise1_BodyNr%d',BodyNr);
        sgt=sgtitle('Reconstruction from noisy Legendre moments')
        sgt.FontSize=25;
        print(gcf,'-dpng',[NewDirName,'/',Name,'.png']);
        
        figure(1)
        Name=sprintf('LegendreNoise0_BodyNr%d',BodyNr);
        sgt=sgtitle('Reconstruction from exact Legendre moments')
        sgt.FontSize=25;
        print(gcf,'-dpng',[NewDirName,'/',Name,'.png']);

    else
        figure(2)       
        Name=sprintf('GeometricNoise1_BodyNr%d',BodyNr);
        sgt=sgtitle('Reconstruction from noisy geometric moments')
        sgt.FontSize=25;
        print(gcf,'-dpng',[NewDirName,'/',Name,'.png']);
        
        figure(1)
        Name=sprintf('GeometricNoise0_BodyNr%d',BodyNr);
        sgt=sgtitle('Reconstruction from exact geometric moments')
        sgt.FontSize=25;
        print(gcf,'-dpng',[NewDirName,'/',Name,'.png']);
                
    end
end

         
            



%Calculation of Nikodym-distance
K=polyshape(x_K,y_K);    
P=polyshape(V(1,:),V(2,:));
KintP=intersect(K,P);
A_KintP=area(KintP);
i
d_N=area(K)+area(P)-2*area(KintP);

  %Number of Terms
    NumTerms=0;
    for k=0:N
        for l=0:N
            NumTerms=NumTerms+1;
            Num2=0;
            for i=1:n
                for s=0:k+l
                    for q1=0:s+1
                        Num2=Num2+(s+2-q1+1);
                    end
                end
            end
            NumTerms=NumTerms+Num2+Num2^2;
        end
    end
 %#terms cost fct.: 
 %#constraints: 5*n
 

     